"__author__ = 'Leo Chan'"
"__credits__ = 'Keboola 2017'"

"""
Python 3 environment 
"""

import pip
pip.main(['install', '--disable-pip-version-check', '--no-cache-dir', 'logging_gelf'])
pip.main(['install', '--disable-pip-version-check', '--no-cache-dir', 'dateparser'])

import sys
import os
import logging
import csv
import json
import pandas as pd
import urllib
import ftplib
#from ftplib import FTP_TLS
import datetime
import dateparser
import io
from keboola import docker

import logging_gelf.formatters
import logging_gelf.handlers

# Environment setup
abspath = os.path.abspath(__file__)
script_path = os.path.dirname(abspath)
os.chdir(script_path)

### Logging
logging.basicConfig(
    level=logging.INFO,
    format='%(asctime)s - %(levelname)s - %(message)s',
    datefmt="%Y-%m-%d %H:%M:%S")
"""
logger = logging.getLogger()
logging_gelf_handler = logging_gelf.handlers.GELFTCPSocketHandler(
    host=os.getenv('KBC_LOGGER_ADDR'),
    port=int(os.getenv('KBC_LOGGER_PORT'))
    )
logging_gelf_handler.setFormatter(logging_gelf.formatters.GELFFormatter(null_character=True))
logger.addHandler(logging_gelf_handler)

# removes the initial stdout logging
logger.removeHandler(logger.handlers[0])
"""

# initialize application
cfg = docker.Config('/data/')

# Access the supplied parameters
params = cfg.get_parameters()
ftp_url = cfg.get_parameters()["ftp_url"]
destination = cfg.get_parameters()["destination"]
username = cfg.get_parameters()["username"]
password = cfg.get_parameters()["#password"]
file_name = cfg.get_parameters()["file_name"]
include_file_name = cfg.get_parameters()["include_file_name"]
include_row_number = cfg.get_parameters()["include_row_number"]
include_date = cfg.get_parameters()["include_date"]
filename_column_name = cfg.get_parameters()["filename_column_name"]
extraction_column_name = cfg.get_parameters()["extraction_column_name"]
row_column_name = cfg.get_parameters()["row_column_name"]
start_date = cfg.get_parameters()["start_date"]
end_date = cfg.get_parameters()["end_date"]
header = cfg.get_parameters()["column_header"]

# Get proper list of tables
cfg = docker.Config('/data/')
out_tables = cfg.get_expected_output_tables()

### setting up date parameters
current_date = dateparser.parse("now")
current_date_string = current_date.strftime("%Y-%m-%d")

### destination to fetch and output files
DEFAULT_FILE_INPUT = "/data/in/tables/"
DEFAULT_FILE_DESTINATION = "/data/out/tables/"

### Setting up audit parameters
if include_file_name == "YES":
    INCLUDE_FILE_NAME = True
else:
    INCLUDE_FILE_NAME = False

if include_date == "YES":
    INCLUDE_DATE = True
else:
    INCLUDE_DATE = False

if include_row_number == "YES":
    INCLUDE_ROW_NUMBER = True
else:
    INCLUDE_ROW_NUMBER = False


def get_tables(in_tables):
    """
    Evaluate input and output table names.
    Only taking the first one into consideration!
    """

    ### input file
    table = in_tables[0]
    in_name = table["full_path"]
    in_destination = table["destination"]
    logging.info("Data table: " + str(in_name))
    logging.info("Input table source: " + str(in_destination))
    if number_of_files == 1:
        return in_name
    elif number_of_files == 2:
        table = in_tables[1]
        in_name_1 = table["full_path"]
        in_destination = table["destination"]
        logging.info("Data table: " + str(in_name))
        logging.info("Input table source: " + str(in_destination))
        return in_name, in_name_1

def get_output_tables(out_tables):
    """
    Evaluate output table names.
    Only taking the first one into consideration!
    """

    ### input file
    table = out_tables[0]
    in_name = table["full_path"]
    in_destination = table["source"]
    logging.info("Data table: " + str(in_name))
    logging.info("Input table source: " + str(in_destination))

    return in_name

def dates_request(start_date, end_date):
    """
    Assemble list of dates based on the settings.
    """

    dates = []
    try:
        start_date_form = dateparser.parse(start_date)
        end_date_form = dateparser.parse(end_date)
        day_diff = (end_date_form-start_date_form).days
        if day_diff < 0:
            logging.error("ERROR: start_date cannot exceed end_date.")
            logging.error("Exit.")
            sys.exit(0)
        temp_date = start_date_form
        day_n = 0
        if day_diff == 0:
            ### normal with leading zeros
            dates.append(temp_date.strftime("%Y-%m-%d"))
            ### no leading zeros
            #dates.append(temp_date.strftime("%Y/%-m/%-d"))
        while day_n < day_diff:
            ### normal with leading zeros 
            dates.append(temp_date.strftime("%Y-%m-%d"))
            ### no leading zeros
            #dates.append(temp_date.strftime("%Y/%-m/%-d"))
            temp_date += datetime.timedelta(days=1)
            day_n += 1
            if day_n == day_diff:
                ### normal with leading zeros
                dates.append(temp_date.strftime("%Y-%m-%d"))
                ### no leading zeros
                #dates.append(temp_date.strftime("%Y/%-m/%-d"))
    except TypeError:
        logging.error("ERROR: Please enter valid date parameters")
        logging.error("Exit.")
        sys.exit(1)

    return dates

def output_file(data_output, file_output, column_headers):
    """ 
    Output the dataframe input to destination file
    Append to the file if file does not exist
    * row by row
    """

    if not os.path.isfile(file_output):
        with open(file_output, "a") as b:
            data_output.to_csv(b, index=False, columns=column_headers)
        b.close()
    else:
        with open(file_output, "a") as b:
            data_output.to_csv(b, index=False, header=False, columns=column_headers)
        b.close()

    return

def find_files():
    """
    Search through the FTP destination and find files which fits the requirements
    Output: a list containing matched file names
    """

    ### Login and cmd to destination
    ftps = ftplib.FTP(ftp_url)
    ftps.login(username,password)
    ftps.cwd(destination)
    list_of_files = ftps.nlst()
    ftps.close()

    dates = dates_request(start_date, end_date)
    date_format = "YYYY-MM-DD"

    matched_files = []
    ### Iternating the requested dates and Match up with files
    for i in dates:
        file_string = file_name.replace(date_format, str(i))

        ### confirm if there are any * in the string
        count = 0
        string_list = []
        for letter in file_string:
            if letter == "*":
                count+=1
        if count == 0:
            ### if no match is found
            string_list.append(string)
        else:
            string_list = file_string.split("*")
        
        bool_exist = False
        for file in list_of_files:
            temp_bool = True
            for part in string_list:
                if part in file:
                    pass
                else:
                    temp_bool =  False
            if temp_bool:
                matched_files.append(file)

    return matched_files

def main():
    """
    Main execution script.
    """
    ### Setting up necessary parameters 
    ftp_link = "ftp://"+str(username)+":"+str(password)+"@"+str(ftp_url)+"/"+str(destination)+"/"

    ### Fetching output file name
    output_filename = get_output_tables(out_tables)

    ### New header with the audit parameterss
    full_header = header
    if INCLUDE_FILE_NAME:
        full_header.append(filename_column_name)
    if INCLUDE_DATE:
        full_header.append(extraction_column_name)
    if INCLUDE_ROW_NUMBER:
        full_header.append(row_column_name)

    ### Setting up a dataframe for output
    df = pd.DataFrame(columns=full_header)

    ### Find the list of files which fits the requested date range or parameters
    matched_list = find_files()
    logging.info("Matched Files: {0}".format(matched_list))

    ### Iternating the request from the list of files which match the requirements
    for file_string in matched_list:
        full_file_link = ftp_link + file_string
        logging.info("Fetching {0}".format(file_string))

        try:
            ### Fetching data via urllib ###
            req = urllib.request.Request(full_file_link)
            response = urllib.request.urlopen(req)
            something = response.read()
            body = io.BytesIO(something)
            output = pd.read_csv(body, dtype=str, encoding='ISO-8859-1')
            logging.info("Downloading {0}".format(file_string))

            ### Local Testing
            #output = pd.read_csv(DEFAULT_FILE_INPUT+file_string, dtype=str)

            ### Audit values
            if INCLUDE_FILE_NAME:
                output[filename_column_name] = file_string
            if INCLUDE_DATE:
                output[extraction_column_name] = current_date_string
            if INCLUDE_ROW_NUMBER:
                output[row_column_name] = output.index.map(int).astype(int)+1
            df = df.append(output)

        except Exception as err:
            logging.warning("Warning: Unable to parse {0} - {1}".format(file_string, err))

    ### Output file
    logging.info("Exporting CSV...")
    output_file(df, output_filename, full_header)

    return
    

if __name__ == "__main__":

    main()

    logging.info("Done.")
