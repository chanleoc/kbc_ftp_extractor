# README #
This repository is a collection of functions which allows users to extract data files from FTP servers with custom audit columns. 

### Configurations ###

1. ftp_url
    - URL of the FTP server which you want to connect
2. destination
    - File path to where you want to fetch your data file
3. username
    - Your FTP connection username
4. password
    - Your FTP connection password
5. file_name
    - The format of the file which you want to fetch
    - YYYY-MM-DD needs to be specify in this format for the program to recognize the date format in the file's name
    - The program will ignore any "*"(star symbol) specified in the file name between the strings around it (No limit in the amount of * uses)
    - *Note*: This program can only support CSV formated files
6. include\_file\_name
    - Adding a column specifying the source file of the row
7. include\_row\_number
    - Adding a column specifying the row's index number in the source file
8. include\_date
    - Adding a column indicating the date of the FTP extraction
9. filename\_column\_name
    - Column name of include\_file\_name in the output file
10. extraction\_column\_name
    - Column name of include\_row\_number in the output file
11. row\_column\_name
    - Column name of include\_date in the output file
12. start_date
    - The start date of the date range you wish the program to search within the FTP server
13. end_date
    - The end date of the date range you wish the program to search within the FTP server
14. column_header
    - The columns from the source file (FTP files) you wish the output file to have

### RAW JSON Configuration ###

```
{
    "ftp_url": "ftp.address.com",
    "destination": "file_1",
    "username": "leochan",
    "#password": "********",
    "file_name": "data YYYY-MM-DD.csv",
    "include_file_name":"YES",
    "include_row_number":"YES",
    "include_date":"YES",
    "filename_column_name":"File_Name",
    "extraction_column_name":"Date_of_Extraction",
    "row_column_name":"Index",
    "start_date": "2017/11/22",
    "end_date": "2017/11/22",
    "column_header":[
        "Column_1",
        "Column_2",
        "Column_3"
    ]
}
```

### Contact Info ###

Leo Chan  
Vancouver, Canada (PST time)  
Email: leo@keboola.com  
Private: cleojanten@hotmail.com  
